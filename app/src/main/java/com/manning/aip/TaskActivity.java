package com.manning.aip;

import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.InputStream;


public class TaskActivity extends ActionBarActivity {
    private Button fetchButton;
    private ImageView mImageView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_task);
        fetchButton = (Button) findViewById(R.id.fetch_button);
        fetchButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                mImageView.setImageBitmap(null);

                //simulate the params you would need to pass in ->
                String strRestoName = "The%20Gage";
                String strCity = "Chicago";
                String strSearchUrl =  String.format("https://ajax.googleapis.com/ajax/services/search/images?v=1.0&q= %s restaurant %s &imgsz=medium&imgtype=photo", strRestoName, strCity);
                strSearchUrl =  strSearchUrl.replaceAll("\\s+","");
                new DownloadImageTask(mImageView).execute(strSearchUrl);
            }
        });
        mImageView = (ImageView) findViewById(R.id.view_set);

    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.task, menu);
        return true;
    }




    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        Intent intent = null;
        switch (item.getItemId()){
           case R.id.action_list:

               intent = new Intent(TaskActivity.this, MyMovies.class);
               finish();
            break;

            default:
                return true;
        }

        startActivity(intent);
        return true;
    }



    private class DownloadImageTask extends AsyncTask<String, Void, Bitmap> {
        ImageView bmImage;

        public DownloadImageTask(ImageView bmImage) {
            this.bmImage = bmImage;
        }

        protected Bitmap doInBackground(String... urls) {


            JSONObject jsonIndividualResult = null;
            try {
                JSONObject jsonRaw = new JSONParser().getSecureJSONFromUrl(urls[0]);

                JSONObject jsonReponse = jsonRaw.getJSONObject("responseData");
                JSONArray jsonArray = jsonReponse.getJSONArray("results");


                jsonIndividualResult = jsonArray.getJSONObject(0);
            } catch (JSONException e) {
                e.printStackTrace();
            }


            Bitmap mIcon11 = null;

            try {
                InputStream in = new java.net.URL(jsonIndividualResult.getString("unescapedUrl")).openStream();
                mIcon11 = BitmapFactory.decodeStream(in);
            } catch (Exception e) {
                //Log.e("Error", e.getMessage());
                e.printStackTrace();
            }
            return mIcon11;
        }

        protected void onPostExecute(Bitmap result) {
            bmImage.setImageBitmap(result);

        }
    }



}

